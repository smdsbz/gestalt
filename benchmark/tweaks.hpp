/**
 * @file tweaks.hpp
 */

#pragma once


namespace gestalt_bench {

/**
 * Only issue requests on valid, i.e. successfully inserted, data.
 */
constexpr bool only_valid_data = true;

}   /* namespace gestalt_bench */
