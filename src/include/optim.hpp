/**
 * @file
 *
 * Optimization switches
 */

#pragma once


namespace gestalt {
namespace optimization {

/**
 * Share RDMA Completion Queue and poll them in batches, merging PCIe requests
 * to RNIC
 * @note negative optimization, if any difference at all ...
 */
constexpr bool batched_poll = false;

/**
 * Proactively sleep for couple of us on high contention to ease uneffective
 * load on remote RNIC
 * @note Only helpful when contention is high and requests are constantly
 * dropped by remote RNIC (e.g. 48 threads on my one-to-one test deployment).
 * Besides this "optimization" is only heuristic and not guaranteed to solve
 * the problem.
 */
constexpr bool retry_holdoff = false;

/**
 * Consider locked however consistent (checksum passed) values, i.e. values that
 * are soon to be overwriten by an on-the-fly write, as usable.
 */
constexpr bool allow_read_transient = true;

/**
 * Upper-layer application directly operates on `bufferlist` provided by Gestalt
 * client handler, so no excessive copy is involved.
 */
constexpr bool use_gestalt_bufferlist = true;

}   /* namespace optimization */
}   /* namespace gestalt */
