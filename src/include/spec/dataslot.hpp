/**
 * @file dataslot.hpp
 *
 * Data structure layout of a slot in headless hashtable.
 *
 * Algorithms are also included in this header file, for in a completely client-
 * centric distributed system, all calculations should be idempotent, and all
 * verifications should follow the same protocal.
 *
 * @note
 * Implementation in this header file is intended to be used by clients, and all
 * memory operations are (or at least, could be) temporal, persistency is not
 * guaranteed! Fast local PMem manipulation support at server side is currently
 * not listed on the roadmap, as this implementation is likely to be only used
 * for benchmarking, and full production feature (like high availability recovery
 * procedure, scrubbing) will remain conceptual :)
 */

#define USING_PMEM false
#ifndef USING_PMEM
#error "USING_PMEM must be specified before #include-ing"
#endif
static_assert(!USING_PMEM);

#pragma once

#include <cstdint>
#include <concepts>
#include <atomic>
#include <cstring>
#include <stdexcept>
#include <isa-l/crc.h>

#include "./params.hpp"


namespace gestalt {

using namespace std;

constexpr size_t DATA_SEG_LEN = params::data_seg_length;

/**
 * Per-slot Metadata Blob
 *
 * __Overview__
 *
 * ```text
 *      +-----------------------------------+
 *   0  |                                   |
 *      |               Key                 |
 *      |                                   |
 *      +-----------------------------------+
 * 496+  0                 8        12       16
 *      +-----------------+--------+--------+
 *      |   Atomic Meta   | Length |  CRC   |
 *      +-----------------+--------+--------+
 * 512B
 * ```
 *
 * Where `CRC` stands for CRC of the entire slot, i.e. value blob, key, atomic
 * meta and effective length. CRC is always calculated as the slot is valid and
 * unlocked, reguardless of its current state.
 *
 * An empty key indicates an invalid slot.
 *
 * If length is larger than `DATA_SEG_LEN`, the KV entry spans accross multiple
 * consecutive slots. Only the first segment records length of the entire entry.
 * In fact, we zero out the `length` field in trailing slots (see bufferlist.hpp).
 *
 * __Atomic Region__
 *
 * ```text
 *  0        8        16       24       32       40       48       56       64b
 * +-----------------------------------+-----------------+--------+--------+
 * |       Key fingerprint (CRC)       |# trailing slots |        |V      L|
 * +-----------------------------------+-----------------+--------+--------+
 * ```
 *
 * Where `V` is valid bit, `L` is lock bit.
 *
 * A key fingerprint is included so we can perform headless CAS on wanted KV,
 * without having to retrieve and compare the entire key with client CPU.
 *
 * Unspecified fields should be zeroed, as the whole 64 bit atomic region is to
 * be CAS-ed and therefore its value must be strictly defined.
 */
struct [[gnu::packed]] dataslot_meta {
    /**
     * Packed C-style string, with handy helpers
     */
    struct [[gnu::packed]] key_type {
        char _k[496];

        /* constructors */

        /**
         * Default empty constructor, constructs an invalid slot metadata.
         */
        key_type() noexcept
        {
            invalidate();
        }
        inline void invalidate() noexcept
        {
            _k[0] = '\0';
        }

        /* don't know why string_view's convert consturctor don't work */
        key_type(const char *k)
        {
            this->set(string_view(k));
        }
        key_type(const string_view &k)
        {
            this->set(k);
        }
        inline key_type &operator=(const string_view &k)
        {
            this->set(k);
            return *this;
        }
        inline void set(const string_view &k)
        {
            if (k.length() > sizeof(key) - 1)
                throw std::invalid_argument("key too long");
            strcpy(this->_k, k.data());
        }
        inline key_type &operator=(const key_type &that) noexcept
        {
            strcpy(this->_k, that._k);
            return *this;
        }

        /* required interfaces */
    public:
        inline const auto c_str() const noexcept
        {
            return _k;
        }
        inline bool operator==(const key_type &that) const noexcept
        {
            return !strcmp(this->_k, that._k);
        }
        inline bool operator!=(const key_type &that) const noexcept
        {
            return !(*this == that);
        }

        static inline uint32_t hash(const string_view &k) noexcept
        {
            return crc32_iscsi((uint8_t*)k.data(), k.length(), 0x114514);
        }
        inline auto hash() const noexcept
        {
            return key_type::hash(_k);
        }

        /* additional helpers */
    public:
        inline bool is_valid() const noexcept
        {
            return !!_k[0];
        }
    } key;

    enum bits_flag : uint8_t {
        none    = 0,
        lock    = 0b00000001,
        valid   = 0b10000000,
    };
    /** atomic metadata */
    mutable union [[gnu::packed]] __atomic_meta_u {
        uint64_t u64;
        struct [[gnu::packed]] __members_s {
            uint32_t key_crc = 0;
            /**
             * number of trailing slots if the entry is multi-slot
             * NOTE: currently always 0, since multi-slot is not implemented
             */
            uint16_t nr_slots = 0;
            uint8_t _ = 0;
            uint8_t bits = bits_flag::none;
        public:
            constexpr __members_s() noexcept = default;
        } m;

        __atomic_meta_u() noexcept : u64(0)
        { }
        __atomic_meta_u(uint32_t crc, bits_flag f = bits_flag::valid) noexcept : u64(0)
        {
            m.key_crc = crc;
            m.bits = f;
        }
    } atomic;

    /** effective length of KV entry */
    uint32_t length;
    /** checksum of the entire current slot */
    uint32_t slot_crc;

    /* constructors */
public:
    /**
     * Default constructor, constructs invalid slot metadata.
     */
    dataslot_meta() noexcept : key(), atomic() {}
    dataslot_meta(const string_view &k) : key(k), atomic(key.hash())
    { }

    /* helpers */
public:
    /**
     * Checks key-related fields.
     * @return
     * * 0 if key-related fields are valid
     * * -EINVAL if key is invalid, i.e. slot is unused
     * * -ECOMM if key is set but CRC does not match
     */
    inline int key_validity() const noexcept
    {
        if (!key.is_valid())
            return -EINVAL;
        if (!(atomic.m.bits & bits_flag::valid))
            return -EINVAL;
        if (key.hash() != atomic.m.key_crc)
            return -ECOMM;
        return 0;
    }
    inline void invalidate() noexcept
    {
        atomic.u64 = 0;
        key.invalidate();
    }

    /**
     * Sets key-related field in metadata
     *
     * The lock bit is clear by default.
     *
     * @param k new key
     */
    inline void set_key(const string_view &k)
    {
        key = k;
        atomic.m.key_crc = key.hash();
        atomic.m.bits = bits_flag::valid;
    }

    /**
     * Check if metadata indicates the slot is currently (write) locked.
     *
     * @note
     * This method only checks bits and nothing else, one should check for
     * validity first and make sure the invocation is thread-safe.
     *
     * @return 
     */
    inline bool is_locked() const noexcept
    {
        return atomic.m.bits & bits_flag::lock;
    }
};
static_assert(std::is_standard_layout_v<dataslot_meta>);
static_assert(sizeof(dataslot_meta::atomic) == 8);
static_assert(sizeof(dataslot_meta) == 512_B);

/**
 * Slot in headless hashtable, packages user data and inline metadata.
 *
 * User data is packed ahead of metadata, for lock bit must be at the end of the
 * slot (see gestalt::dataslot_meta ).
 */
struct [[gnu::packed]] dataslot {
    using meta_type = dataslot_meta;
    using key_type = dataslot_meta::key_type;

    /**
     * Packed buffer, with handy helpers
     */
    struct [[gnu::packed]] value_type {
        uint8_t _d[DATA_SEG_LEN];

        /* constructors */
    public:
        value_type() noexcept {}
        /**
         * Initialize with given data
         * @param d source buffer
         * @param len length to be copied
         */
        value_type(const void *d, size_t len)
        {
            this->set(d, len);
        }
        value_type(const value_type &) = delete;

        /* helpers */
    public:
        /**
         * Assigns data
         *
         * @note
         * Unused part of the buffer should be zeroed-out, for data length is
         * not recorded here (or rather, due to our large-KV-spans-across-multiple-
         * consecutive-slots-design, valid segment size of the current slot is
         * not recorded at all), so we checksum on the entire block.
         *
         * @param d source data buffer
         * @param len length to be copied
         */
        inline void set(const void *d, size_t len)
        {
            if (len > sizeof(_d))
                throw std::invalid_argument("too large");
            memcpy(_d, d, len);
            memset(_d + len, 0, sizeof(_d) - len);
        }
        /**
         * Get buffer
         * @return raw buffer pointer
         */
        inline auto get() noexcept
        {
            return _d;
        }
    } data;

    meta_type meta;

    /* constructors */
public:
    /**
     * Default constructor, constructs invalid / unused slot.
     */
    dataslot() noexcept : meta() {}
    dataslot(const string_view &k, const void *d, size_t dlen)
    {
        reset(k, d, dlen);
    }
    void reset(const string_view &k, const void *d, size_t dlen)
    {
        meta.set_key(k);    // sets valid bit as well
        data.set(d, dlen);
        meta.length = dlen;
        meta.slot_crc = calc_slot_crc();
    }
    /**
     * Re-format dataslot using data already in slot
     * @param dlen length of data
     */
    void reformat(size_t dlen)
    {
        if (dlen > sizeof(data))
            throw std::invalid_argument("too large");
        meta.length = dlen;
        /* ensure unused region is cleared, otherwise CRC will differ */
        memset(data.get() + dlen, 0, sizeof(data) - dlen);
        meta.slot_crc = calc_slot_crc();
    }
    /**
     * Calculate slot CRC
     *
     * CRC is always calculated as the slot is valid and unlocked.
     *
     * @note for convenience of implementation, the slot's state is temporarily
     * altered during calculation.
     *
     * @return calculated slot CRC
     */
    inline uint32_t calc_slot_crc() const noexcept
    {
        const auto original_flags = meta.atomic.m.bits;
        meta.atomic.m.bits = meta_type::bits_flag::valid;
        uint32_t slot_crc = crc32_iscsi((uint8_t*)this, sizeof(*this) - sizeof(meta.slot_crc), 0x1919810);
        meta.atomic.m.bits = original_flags;
        return slot_crc;
    }

    /* required interface */
public:
    inline const key_type &key() const noexcept
    {
        return meta.key;
    }
    inline value_type &value() noexcept
    {
        return data;
    }
    inline size_t size() const noexcept
    {
        return meta.length;
    }

    inline void invalidate() noexcept
    {
        meta.invalidate();
    }
    inline bool is_valid() const noexcept
    {
        return (
                meta.key_validity() == 0
            && calc_slot_crc() == meta.slot_crc
        );
    }
    inline bool is_invalid() const noexcept
    {
        return !is_valid();
    }

    /* helpers */
public:
    /**
     * Check slot validity
     * @return
     * * 0 slot is valid, ready to be read
     * * -EINVAL if invalid or unused
     * * -ECOMM if slot checksum does not match
     * * -EAGAIN valid and consistent, but currently locked, meaning that the
     *      value is soon to be changed
     */
    inline int validity() const noexcept
    {
        if (auto kv = meta.key_validity(); kv)
            return kv;
        if (calc_slot_crc() != meta.slot_crc)
            return -ECOMM;
        if (meta.is_locked())
            return -EAGAIN;
        return 0;
    }
};
static_assert(std::is_standard_layout_v<dataslot>);
/* ensure meta.slot_crc is the last field of dataslot */
static_assert(offsetof(dataslot, meta.slot_crc) == sizeof(dataslot) - sizeof(dataslot().meta.slot_crc));
static_assert((sizeof(dataslot) % 512_B) == 0);

}   /* namespace gestalt */


template<>
struct std::hash<gestalt::dataslot::key_type> {
    inline std::size_t operator()(const gestalt::dataslot::key_type &k) const
    {
        return k.hash();
    }
};
