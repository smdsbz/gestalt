Gestalt
=======

How to setup a development environment
--------------------------------------

On your dev machine, i.e. the vm host

0. install Vagrant and some handy plugin

    ```console
    $ vagrant plugin install vagrant-hosts
    ```

1. create vms

    ```console
    $ vagrant up
    ```

2. add vm to SSH config

    ```console
    $ vagrant ssh-config >> ~/.ssh/config
    ```

3. provision

    ```console
    $ ansible-playbook ansible/playbook/vbox-provision.yml
    ```

> Note
>
> Ansible is configured to run on the dev env by default.


How to run
----------

### Deploy

On the control machine, do the following under project root

0.  if you're running Gestalt on cluster other than the dev env, construct your own inventory and use the `-i` option to redirect Ansible

1.  download Boost and YCSB to project root

    ```console
    $ wget https://boostorg.jfrog.io/artifactory/main/release/1.80.0/source/boost_1_80_0.tar.gz
    $ wget https://github.com/brianfrankcooper/YCSB/releases/download/0.17.0/ycsb-0.17.0.tar.gz
    ```

2.  pull all the dependencies and build Gestalt

    ```console
    $ ansible-playbook ansible/playbook/deploy-gestalt.yml
    ```

### Run benchmark

```console
$ ansible-playbook ansible/playbook/benchmark.yml
```

and follow the prompt. Benchmark result will be in ansible output.

> Note
>
> The `benchmark.yml` play always sync source and rebuild before running
> any benchmark, so you don't have to re-play `deploy-gestalt.yml` unless you
> updated CMake files.

> Note
>
> You may find `-e "gestalt_debug_log_level=debug"` useful for debugging.

If anything goes wrong, be sure to run

```console
$ ansible-playbook ansible/playbook/force_stop_all.yml
```

to stop all Gestalt processes before continuing.

